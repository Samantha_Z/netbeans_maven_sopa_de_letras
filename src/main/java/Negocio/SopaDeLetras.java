package Negocio;
//Librerias

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.io.font.FontConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author (your name)
 */
public class SopaDeLetras {

    //sopa que lee
    private char sopas[][];
    //sopa de boolean para pintrar
    private boolean[][] s;
    //validación para saber si la palabra está
    private boolean esta;
    //variable que retorna las todas las posiciones
    public String rts;

    //CONSTRUCTORES
    public SopaDeLetras() {

    }

    public SopaDeLetras(char[][] sopas) {
        this.sopas = sopas;
        rts = "";
        //inicialzar la sopas de booleanos con el tamaño de la sopa de excel
        s = new boolean[sopas.length][sopas[0].length];
    }

    /*CREACIÓN DEL PDF*/
    public void crearPDF() throws FileNotFoundException, IOException {
        String dest = "C:\\pdf\\sopaExcel.pdf";

        PdfWriter writer = new PdfWriter(dest);
        PdfDocument pdf = new PdfDocument(writer);

        Document document = new Document(pdf, PageSize.A4);
        document.setMargins(35, 25, 30, 25);
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);

        Table table = new Table(sopas[0].length);
        
        pintar(font, table);
        table.setWidth(100);

        document.add(table);
        //setFixedPosition(250,700, 200
        document.add(new Paragraph(rts));
        document.close();
        mostrarPdf(dest);
    }

    /*MOSTRAR EL PDF*/
    public void mostrarPdf(String ruta) {

        try {
            String exe = "rundll32 url.dll,FileProtocolHandler " + ruta;

            Process p = Runtime.getRuntime().exec(exe);
            p.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(SopaDeLetras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SopaDeLetras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*METODO QUE RECORRE LA MATRIZ Y PINTA LAS CELDAS*/
    private void pintar(PdfFont font, Table tabla) {
        

        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if (s[i][j] == true) {
                    tabla.addCell(String.valueOf(sopas[i][j])).setFont(font);
                    tabla.getCell(i, j).setBackgroundColor(Color.BLUE);
                    tabla.getCell(i, j).setFontColor(Color.LIGHT_GRAY);

                } else {
                    tabla.addCell(String.valueOf(sopas[i][j])).setFont(font).setBackgroundColor(Color.LIGHT_GRAY);
                }
            }
        }
    }

    
    public SopaDeLetras(String palabras) throws Exception {
        
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }
        s = new boolean[this.sopas.length][this.sopas[0].length];
    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "\n";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    /*MÉTODO QUE COMPRUEBA SI EL NÚMERO DE FILAS Y  COLUMNAS SON EL MISMO*/
    public boolean esCuadrada() throws Exception {

        if (this.sopas == null) {
            throw new Exception("Error, la matriz no es cuadrada");
        }

        int n = this.sopas.length;

        for (int i = 0; i < this.sopas.length; i++) {
            if (n != this.sopas[i].length) {
                return false;
            }
        }
        return true;
    }

    /*MÉTODO QUE COMPRUEBA SI EL NÚMERO DE FILAS Y  COLUMNAS ES DIFERENTE*/
    public boolean esRectangular() throws Exception {

        if (this.sopas == null) {
            throw new Exception("Error, la matriz esta vacía");
        }

        int columna = sopas[0].length;

        for (int j = 0; j < this.sopas[0].length; j++) {
            for (int i = 0; i < this.sopas.length; i++) {
                if (columna == sopas.length || columna != sopas[i].length) {
                    return false;
                }
            }
        }
        return true;
    }

    /*MÉTODO QUE COMPRUEBA SI LA MATRIZ ES DISPERSA*/
    public boolean esDispersa() throws Exception {
        return (!esRectangular() && !esCuadrada());
    }

    /*MÉTODO QUE RETORNA CUANTAS VECES ESTA LA PALABRA EN LA MATRIZ*/
    public int getContar(String palabra) {
       
        int cont = 0;
        int posicion;

        char vector[] = palabra.toCharArray();

        for (int i = 0; i < this.sopas.length; i++) {
            posicion = 0;
            for (int j = 0; j < this.sopas[0].length; j++) {

                if (sopas[i][j] == vector[posicion]) {
                    posicion++;

                    if (posicion == vector.length) {
                        cont++;
                        posicion = 0;
                        j -= vector.length - 1;
                    }
                } else {
                    posicion = 0;
                }
            }
        }
        return cont;
    }

    /*MÉTODO RETORNA LA DIAGONAL PRINCIPAL SI LA MATRIZ ES CUADRADA*/
    public char[] getDiagonalPrincipal() throws Exception {

        if (!esCuadrada()) {
            throw new Exception("Error, no se puede obtener la diagonal porque la matriz no es cuadrada");
        }

        char diagoPrincipal[] = new char[sopas.length];

        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if (i == j) {

                    diagoPrincipal[i] = sopas[i][j];

                }
            }
        }

        return diagoPrincipal;
    }

    /*MÉTODO RETORNA LA MATRIZ*/
    public char[][] getSopas() {
        return this.sopas;
    }

    /*METODO QUE BUSCA LA PALABRA EN TODAS LAS POSICIONES*/
    public void verificar(String palabra) {

        //variable que verifica si la palabra esta
        esta = false;

        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {

                horizontalDerecha(i, j, palabra);

                horizontalIzquierda(i, j, palabra);

                verticalSuperior(i, j, palabra);

                verticalInferior(i, j, palabra);

                principalinferior(i, j, palabra);

                principalsuperior(i, j, palabra);

                inversaSuperior(i, j, palabra);

                inversaInferior(i, j, palabra);
            }

        }
        if (esta == false) {
            rts = "NO SE ENCONTRÓ LA PALABRA.";
        } else {
            rts = "SE ENCONTRÓ LA PALABRA: " + palabra + "\n" + rts;
        }
    }

    /* METODO QUE BUSCA LA PALABRA EN IZQUIERDA A DERECHA*/
    private boolean horizontalDerecha(int a, int b, String palabra) {
        int aux = 0;
        int a1 = a;
        int b1 = b;

        for (int j = b; j < sopas[a].length; j++) {
            if (sopas[a][j] == palabra.charAt(aux)) {
                aux++;
            } else {
                return false;
            }
            if (aux == palabra.length()) {
                esta = true;
                for (int i = 0; i < palabra.length(); i++) {
                    s[a][j] = true;
                    j--;
                }
                rts += "En la fila: " + a1 + "  y la columna: " + b1 + "  de izquierda a derecha" + "\n";
                return true;
            }
        }
        return true;
    }

    /*METODO QUE BUSCA LA PALABRA EN DERECHA A IZQUIERA*/
    private void horizontalIzquierda(int a, int b, String palabra) {

        int aux = 0;
        int a1 = a;
        int b1 = b;
        for (int j = b; j >= 0; j--) {
            if (sopas[a][j] == palabra.charAt(aux)) {
                aux++;
            } else {
                return;
            }
            if (aux == palabra.length()) {
                //Pintar las casillas
                for (int i = 0; i < palabra.length(); i++) {
                    s[a][j] = true;
                    j++;
                }
                esta = true;
                rts += "En la fila: " + a1 + "   y la columna: " + b1 + "  de derecha a izquierda" + "\n";
                return;
            }
        }
    }

    /*METODO QUE BUSCA LA PALABRA DE ABAJO HACIA ARRIBA*/
    private void verticalSuperior(int a, int b, String palabra) {

        int aux = 0;
        int a1 = a;
        int b1 = b;

        for (int i = a; i >= 0; i--) {
            if (sopas[i][b] == palabra.charAt(aux)) {
                aux++;
            } else {
                return;
            }
            if (aux == palabra.length()) {

                esta = true;
                for (int p = 0; p < palabra.length(); p++) {
                    s[i][b] = true;
                    i++;
                }
                rts += "En la fila:  " + a1 + "  y la columna: " + b1 + "  de abajo hacia arriba" + "\n";
                return;
            }
        }
    }

    /* METODO QUE BUSCA LA PALABRA DE ARRIBA HACIA ABAJO */
    private void verticalInferior(int a, int b, String palabra) {
        int aux = 0;
        int a1 = a;
        int b1 = b;

        for (int i = a; i < sopas.length; i++) {
            if (sopas[i][b] == palabra.charAt(aux)) {
                aux++;
            } else {
                return;
            }
            if (aux == palabra.length()) {

                for (int p = 0; p < palabra.length(); p++) {
                    s[i][b] = true;
                    i--;
                }
                esta = true;
                rts += "En la fila:  " + a1 + "  y la columna: " + b1 + "  de arriba hacia abajo" + "\n";
                return;
            }
        }
    }

    /* METODO QUE BUSCA LA PALABRA SOBRE LA DIAGONAL PRINCIPAL SUPERIOR*/
    private void principalsuperior(int a, int b, String palabra) {
        
        int aux = 0;
        int a1 = a;
        int b1 = b;
        while (a >= 0 && b >= 0) {
            if (palabra.charAt(aux) == sopas[a][b]) {
                aux++;
            } else {
                return;
            }

            if (aux == palabra.length()) {

                for (int p = 0; p < palabra.length(); p++) {
                    s[a][b] = true;
                    a++;
                    b++;
                }
                esta = true;
                rts += "En la fila:  " + a1 + "  y la columna: " + b1 + "  hacia la diagonal principal superior " + "\n";
                return;
            }
            a--;
            b--;
        }
    }

    /* METODO QUE BUSCA LA PALABRA SOBRE LA DIAGONAL PRINCIPAL INFERIROR*/
    private void principalinferior(int a, int b, String palabra) {
        
        int aux = 0;
        int a1 = a;
        int b1 = b;

        while (a < sopas.length && b < sopas[0].length) {
            if (palabra.charAt(aux) == sopas[a][b]) {
                aux++;
            } else {
                return;
            }
            if (aux == palabra.length()) {
                for (int p = 0; p < palabra.length(); p++) {
                    s[a][b] = true;
                    a--;
                    b--;
                }
                esta = true;
                rts += "En la fila:  " + a1 + "  y la columna: " + b1 + "  hacia la diagonal principal inferior " + "\n";
                return;
            }
            a++;
            b++;

        }
    }

    /* METODO QUE BUSCA LA PALABRA SOBRE LA DIAGONAL INVERSA SUPERIOR*/
    private void inversaSuperior(int a, int b, String palabra) {
        int aux = 0;
        int a1 = a;
        int b1 = b;

        while (a >= 0 && b < sopas[0].length) {
            if (palabra.charAt(aux) == sopas[a][b]) {
                aux++;
            } else {
                return;
            }

            if (aux == palabra.length()) {
                for (int p = 0; p < palabra.length(); p++) {
                    s[a][b] = true;
                    a++;
                    b--;
                }
                esta = true;
                rts += "En la fila:  " + a1 + "  y la columna:  " + b1 + "   hacia la diagonal inversa superior" + "\n";
                return;
            }
            a--;
            b++;
        }
    }

    /* METODO QUE BUSCA LA PALABRA SOBRE LA DIAGONAL INVERSA INFERIROR */
    private void inversaInferior(int a, int b, String palabra) {
        
        int aux = 0;
        int a1 = a;
        int b1 = b;

        while (a < sopas.length && b >= 0) {
            if (palabra.charAt(aux) == sopas[a][b]) {
                aux++;
            } else {
                return;
            }

            if (aux == palabra.length()) {
                for (int p = 0; p < palabra.length(); p++) {
                    s[a][b] = true;
                    a--;
                    b++;
                }
                esta = true;
                rts += "En la fila:  " + a1 + "  y la columna: " + b1 + "  hacia la diagonal inversa inferior" + "\n";
                return;
            }
            a++;
            b--;
        }
    }

}
